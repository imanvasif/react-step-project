import React, { Component } from "react";
import Navbar from "./components/Navbar/Navbar";
import SingleNot from "./components/SingleNot/SingleNot";
import Actual from "./components/Actual/Actual";
import { Route, Switch } from "react-router-dom";
import CreatePage from "./components/CreatePage/CreatePage";
export default class App extends Component {


  render() {
    // state = []


    return (
      <div className="app">
        <Navbar clickToButton={this.clickButton} />
        <Switch>
          <Route path="/"  exact component={Actual} />
          <Route path="/Archive" component={SingleNot} />          
          <Route path="/Create" component={CreatePage} />
        </Switch>
      </div>
    );
  }
}
