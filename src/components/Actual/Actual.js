import React, { Component } from "react";
import "./Actual.scss";

class Actual extends Component {
  state = {
    texts: [],

  };

  componentDidMount() {
    fetch("http://localhost:3001/Texts")
      .then((r) => r.json())
      .then((data) => this.setState({ texts: data }));
  }

  render() {
    const {id} = this.props
    return (
      <div className="actual-area">
        {this.state.texts.map((getText,) => (
          <div
            className="actual-item"
            style={{ backgroundColor: getText.backgroundColor }}
            key={id}
          >
            <h3 className="actual-h3" key= {id} >{getText.head}</h3>
            <div className="actual-text" key= {id} >{getText.body}</div>
          </div>
        ))}
      </div>
    );
  }
}
export default Actual;
