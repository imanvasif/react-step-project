import React, { Component } from "react";
import "./CreatePage.scss";
import axios from 'axios';

export default class CreatePage extends Component {
    state = {
      backColor: "white",
      color: "black",
    };
    state={
      id: '',
      head: '',
      body: '',
      backgroundColor: ''
  }
  

  

  blueColor = () => {
    this.setState({
      backColor: "blue",
      color: "white",
    });
  };
  redColor = () => {
    this.setState({
      backColor: "red",
    });
  };
  greenColor = () => {
    this.setState({
      backColor: "green",
    });
  };
  yellowColor = () => {
    this.setState({
      backColor: "yellow",
    });
  };
  defaultColor = () => {
    this.setState({
      backColor: "white",
    });
  };

  changeHandler = (event) => {
    this.setState({
        [event.target.name]: event.target.value
    })
}
clickSubmit = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://localhost:3001/Texts', this.state)
    .then(r => console.log(r)
    )
    alert("Your notes saved... Thanks  for writing :)")
    
}

  render() {
    const{id,head, body, backgroundColor}= this.state
    return (
      <div className="create-page">
        <div className="create-area">
          <h2 className="create-h2">Fill Data</h2>
          <form className="create-form">
            <input
              className="create-input"
              placeholder="Title"
              style={{
                backgroundColor: this.state.backColor,
                color: this.state.backcColor,
              }}
              name="head"
              value= {head} 
              onChange={this.changeHandler}
            />
            <textarea
              style={{ backgroundColor: this.state.backColor }}
              className="create-text"
              placeholder="Note text"
              name="body"
              value= {body} 
              onChange={this.changeHandler}
            ></textarea>
            <input type="text" style={{ backgroundColor: this.state.backColor }} className="textId" value={id} placeholder="Write Id" name="id" onChange={this.changeHandler} />
            <input type="text" style={{ backgroundColor: this.state.backColor }} className="textId" value={backgroundColor} placeholder="Write color" name="backgroundColor" onChange={this.changeHandler}/>
            <div className="color">
              <p className="pcol"> Color</p>
              <div className="color-item1" onClick={this.blueColor}></div>
              <div className="color-item2" onClick={this.redColor}></div>
              <div className="color-item3" onClick={this.greenColor}></div>
              <div className="color-item4" onClick={this.yellowColor}></div>
              <div className="color-item5" onClick={this.defaultColor}></div>
            </div>
            <button type="submit" className="btn" onClick ={this.clickSubmit} >
              Create
            </button>
          </form>
        </div>
      </div>
    );
  }
}
