import React from 'react';
import "./Navbar.scss";
import {NavLink} from "react-router-dom";
import shakhtar from "../../logo/shakhtar.png";
import PropTypes from "prop-types";
 function Navbar(props) {
    return (
        <div className = "header">
           <div className="image">
           <img src={shakhtar} alt="logo" className="shaktar"  />
            <p className="notesapp"> NotesApp </p>
            </div>
            <div className="nav-link">
                <NavLink className="link-item  " exact to="/"  onClick={props.clickToButton} > Actual</NavLink>
                <NavLink className="link-item" to="/archive"  onClick={props.clickToButton}  >Archive</NavLink>
                <NavLink className="link-item" to="/create"  onClick={props.clickToButton} >Create</NavLink>
            </div>
        </div>
    
    )
}
Navbar.propTypes = {
    clickToButton: PropTypes.func
}
export default Navbar;