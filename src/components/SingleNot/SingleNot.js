import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';
import "./SingleNot.scss";
import axios from 'axios'

class SingleNot extends Component {
    state={
        isShowPage: false,
        deletePages: "block",
    }
    state={
        id: '',
        head: '',
        body: '',
        backgroundColor: '',
        color: ''
    }
   
    openModal = (e) => {
       
        this.setState({
         isShowPage: !this.state.isShowPage
        })
        e.preventDefault()
    }
    closeModalPage =(el) => {
        if(this.state.isShowPage === true){
            this.setState({
                isShowPage:!this.state.isShowPage
            })
            el.preventDefault()
        }
        
    }

    deletePage = (elem) => {
        if(this.state.isShowPage === true){
        this.setState({
            deletePages: "none",
            isShowPage: !this.state.isShowPage
        })
        elem.preventDefault()
        alert(" Your Notes was deleted...")
    }}
    changeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    clickSubmit = e => {
        e.preventDefault()
        console.log(this.state)
        axios.post('http://localhost:3001/Texts', this.state)
        .then(r => console.log(r)
        )
        
    }
 
    render() {
        const{id,head, body,}= this.state
        return (
                
                <div className="not-area" >
                    
                    <div className = "main-zone" >
                        <div className="mzone"  style={{display: this.state.deletePages}} >
                        <input className="not-h1" type="text" value= {head} placeholder="Title" name="head" onChange={this.changeHandler} /><hr/>
                        <textarea className="not-input" value ={body} placeholder="Write note" name="body" onChange={this.changeHandler}></textarea>
                        <input type="text" className="textId" value={id} placeholder="Write Id" name="id" onChange={this.changeHandler}></input>
                        </div>
                    </div>
                    
                    <form className="btn-zone"  >
                        <button type ="submit" className ="not-btn" onClick ={this.clickSubmit} > EDIT </button>
                        <button type ="submit" className ="not-btn"  > ARCHIVE </button>
                        <button type ="submit" className ="not-btn" onClick ={this.openModal} > DELETE </button>
                        <Modal showPage={this.state.isShowPage} closeModal={this.closeModalPage} deleteForm={this.deletePage} />

                    </form>

                </div>
            
        )
    }
}
SingleNot.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    clickSubmit: PropTypes.func
}
export default SingleNot