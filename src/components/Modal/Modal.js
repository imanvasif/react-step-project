import React  from 'react'
import PropTypes from 'prop-types'
import './Modal.scss'

 const Modal =(props) => {

        return (
            <div className="modal-area" style ={(props.showPage) ? {display: "block"} : {display: "none"} } >
                <div className="modal-page" >
                    <p className="modal-title" >Delete this note?</p>
                    <div className= "btns">
                        <button type="submit" className = "btn1" onClick={props.deleteForm}  >OK</button>
                        <button type="submit" className = "btn2" onClick={props.closeModal} >Cancel</button>
                    </div>
                </div>
            </div>
        )
    
}
Modal.propTypes={
    showPage: PropTypes.bool,
    closeModal: PropTypes.func,
    deleteForm:PropTypes.func

}
export default Modal;